using System;
using System.Drawing;


namespace AssignmentThree;

class main
{
    static void Main(String[] args)
    {
        try
        {
            String path ="../../../Images/tree.jpg";
            Bitmap bmp = new Bitmap(path);
            convert.convert24bitTo8bit(bmp);
        }
        catch (FileNotFoundException )
        {
            Console.WriteLine("can't read Path of Image");
            throw;
        }
        
        
    }
}
